# encoding: utf-8
require 'spec_helper'

describe ISO20022::SEPAAccount do
  describe :new do
    it 'should not accept unknown keys' do
      expect {
        ISO20022::SEPAAccount.new foo: 'bar'
      }.to raise_error(NoMethodError)
    end
  end

  describe :name do
    it 'should accept valid value' do
      expect(ISO20022::SEPAAccount).to accept('Gläubiger GmbH', 'Zahlemann & Söhne GbR', 'X' * 70, for: :name)
    end

    it 'should not accept invalid value' do
      expect(ISO20022::SEPAAccount).not_to accept(nil, '', 'X' * 71, for: :name)
    end
  end

  describe :iban do
    it 'should accept valid value' do
      expect(ISO20022::SEPAAccount).to accept('DE21500500009876543210', 'PL61109010140000071219812874', for: :iban)
    end

    it 'should not accept invalid value' do
      expect(ISO20022::SEPAAccount).not_to accept(nil, '', 'invalid', for: :iban)
    end
  end

  describe :bic do
    it 'should accept valid value' do
      expect(ISO20022::SEPAAccount).to accept('DEUTDEFF', 'DEUTDEFF500', 'SPUEDE2UXXX', for: :bic)
    end

    it 'should not accept invalid value' do
      expect(ISO20022::SEPAAccount).not_to accept('', 'invalid', for: :bic)
    end
  end
end


describe ISO20022::CHAccount do
  describe :new do
    it 'should not accept unknown keys' do
      expect {
        ISO20022::SEPAAccount.new foo: 'bar'
      }.to raise_error(NoMethodError)
    end
  end

  describe :name do
    it 'should accept valid value' do
      expect(ISO20022::CHAccount).to accept('Gläubiger GmbH', 'Zahlemann & Söhne GbR', 'X' * 70, for: :name)
    end

    it 'should not accept invalid value' do
      expect(ISO20022::CHAccount).not_to accept(nil, '', 'X' * 71, for: :name)
    end
  end

  describe :iban do
    it 'should accept valid value' do
      expect(ISO20022::CHAccount).to accept('DE21500500009876543210', 'PL61109010140000071219812874', for: :iban)
    end

    it 'should not accept invalid value' do
      expect(ISO20022::CHAccount).not_to accept(nil, '', 'invalid', for: :iban)
    end
  end

  describe :clearingcode do
    it 'should accept valid value' do
      expect(ISO20022::CHAccount).to accept('CHBCC', for: :clearingcode)
    end

    it 'should not accept invalid value' do
      expect(ISO20022::CHAccount).not_to accept('', for: :clearingcode)
    end
  end

  describe :clearingid do
    it 'should accept valid value' do
      expect(ISO20022::CHAccount).to accept('235', for: :clearingid)
    end

    it 'should not accept invalid value' do
      expect(ISO20022::CHAccount).not_to accept('', for: :clearingid)
    end
  end
end

#udeb_name: 'FRITZ MUELLER',
#udeb_street: 'ROSENWEG',
#udeb_streetnr: '6',
#udeb_postcode: '3110',
#udeb_town: 'MUENSINGEN'
