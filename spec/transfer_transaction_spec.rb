# encoding: utf-8
require 'spec_helper'

describe ISO20022::SEPATransaction do
  describe :initialize do
    it 'should initialize a valid transaction' do
      expect(
        ISO20022::SEPATransaction.new name:                   'Telekomiker AG',
                                      iban:                   'DE37112589611964645802',
                                      bic:                    'PBNKDEFF370',
                                      amount:                 102.50,
                                      reference:              'XYZ-1234/123',
                                      remittance_information: 'Rechnung 123 vom 22.08.2013'
      ).to be_valid
    end
  end

  context 'Requested date' do
    it 'should allow valid value' do
      expect(ISO20022::SEPATransaction).to accept(nil, Date.new(1999, 1, 1), Date.today, Date.today.next, Date.today + 2, for: :requested_date)
    end

    it 'should not allow invalid value' do
      expect(ISO20022::SEPATransaction).not_to accept(Date.new(1995,12,21), Date.today - 1, for: :requested_date)
    end
  end
end


describe ISO20022::CHTransaction do
  describe :initialize do
    it 'should initialize a valid transaction' do
      expect(
        ISO20022::CHTransaction.new   name:                   'Telekomiker AG',
                                      adrline1:               'Waldweg 1',
                                      adrline2:               '7430 Thusis',
                                      iban:                   'DE37112589611964645802',
                                      amount:                 102.50,
                                      reference:              'XYZ-1234/123',
                                      remittance_information: 'Rechnung 123 vom 22.08.2013'
      ).to be_valid
    end
  end

  context 'Requested date' do
    it 'should allow valid value' do
      expect(ISO20022::CHTransaction).to accept(nil, Date.new(1999, 1, 1), Date.today, Date.today.next, Date.today + 2, for: :requested_date)
    end

    it 'should not allow invalid value' do
      expect(ISO20022::CHTransaction).not_to accept(Date.new(1995,12,21), Date.today - 1, for: :requested_date)
    end
  end
end
