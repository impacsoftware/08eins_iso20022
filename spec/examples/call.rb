=begin
strans = ISO20022::SEPATransfer.new(
  name: 'MUSTER AG',
  bic: 'RABONL2U',
  iban: 'NL08RABO0135742099'
)
strans.add_transaction(
  name: 'Zahlemann & Söhne GbR',
  bic: 'SPUEDE2UXXX',
  iban: 'DE21500500009876543210',
  amount: 32.99,
  instruction: '332323',
  reference: 'XYZ/2013-08-ABO/6789'
)

outfile = File.open("sepa.xml", "w")
outfile.puts(strans.to_xml())
outfile.close
#puts strans.to_xml()


chtrans = ISO20022::CHTransfer.new(
  name: 'MUSTER AG',
  iban: 'CH8904835098765432000',
  clearingcode: 'CHBCC',
  clearingid: '4835'
  #udeb_name: 'FRITZ MUELLER',
  #udeb_street: 'ROSENWEG',
  #udeb_streetnr: '6',
  #udeb_postcode: '3110',
  #udeb_town: 'MUENSINGEN'
)
chtrans.add_transaction(
  name: 'HOCHALPINES INSTITUT FTAN',
  #street: 'WALDWEG',
  #streetnr: '1',
  #postcode: '7551',
  #town: 'FTAN',
  #bic: 'GRKBCH2270A',
  adrline1: 'Waldweg 1',
  adrline2: '7430 Thusis',
  iban: 'CH3808888123456789012',
  amount: 32.99,
  instruction: '332323',
  reference: 'XYZ/2013-08-ABO/6789',
  remittance_information: 'Mitteilung an den Zahlungsempfänger',
  requested_date: '1977-12-22'
)

chtrans.add_transaction(
  name: 'HOCHALPINES INSTITUT FTAN 2',
  #street: 'WALDWEG',
  #streetnr: '1',
  #postcode: '7551',
  #town: 'FTAN',
  #bic: 'GRKBCH2270A',
  adrline1: 'Waldweg 2',
  adrline2: '7430 Thusis',
  iban: 'CH3808888123456789012',
  amount: 22.99,
  instruction: '3323232222',
  reference: 'XYZ/2013-08-ABO/678922222',
  remittance_information: 'Mitteilung an den Zahlungsempfänger',
  requested_date: '1977-12-22'
)

#puts chtrans.to_xml()
outfile = File.open("chtrans.xml", "w")
outfile.puts(chtrans.to_xml())
outfile.close
=end