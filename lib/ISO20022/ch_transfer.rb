# encoding: utf-8
module ISO20022
  class CHTransfer < Message
    self.account_class = CHAccount
    self.transaction_class = CHTransaction
    self.xml_main_tag = 'CstmrCdtTrfInitn'
    self.known_schemas = [ PAIN_001_001_03 ]

  private
    # Find groups of transactions which share the same values of some attributes
    def transaction_group(transaction)
      { requested_date: transaction.requested_date,
        batch_booking:  transaction.batch_booking
      }
    end

    def build_payment_informations(builder)
      # Build a PmtInf block for every group of transactions
      grouped_transactions.each do |group, transactions|
        # All transactions with the same requested_date are placed into the same PmtInf block
        builder.PmtInf do
          builder.PmtInfId(payment_information_identification(group))
          builder.PmtMtd('TRF')
          builder.BtchBookg(group[:batch_booking])
          #builder.NbOfTxs(transactions.length)
          builder.CtrlSum('%.2f' % amount_total(transactions))
          builder.ReqdExctnDt(Date.parse(group[:requested_date]).iso8601)
          builder.Dbtr do
            builder.Nm(account.name)
          end
          builder.DbtrAcct do
            builder.Id do
              builder.IBAN(account.iban)
            end
          end
          builder.DbtrAgt do
            builder.FinInstnId do
              builder.ClrSysMmbId do
                builder.ClrSysId do
                  builder.Cd(account.clearingcode)
                end
                builder.MmbId(account.clearingid)
              end
            end
          end
          
          udeb =!account.udeb_street.nil? && !account.udeb_streetnr.nil? && !account.udeb_postcode.nil? && !account.udeb_town.nil? && !account.udeb_country.nil?
          
          if udeb
            builder.UltmtDbtr do
              builder.PstlAdr do
                builder.StrtNm(account.udeb_street)
                builder.BldgNb(account.udeb_streetnr)
                builder.PstCd(account.udeb_postcode)
                builder.TwnNm(account.udeb_town)
                builder.Ctry(account.udeb_country)
              end
            end
          end
          transactions.each do |transaction|
            build_transaction(builder, transaction)
          end
        end
      end
    end

    def build_transaction(builder, transaction)
      builder.CdtTrfTxInf do
        builder.PmtId do
          if transaction.instruction.present?
            builder.InstrId(transaction.instruction)
          end
          if transaction.reference.present?
            builder.EndToEndId(transaction.reference)
          end
        end
        builder.Amt do
          builder.InstdAmt('%.2f' % transaction.amount, Ccy: 'CHF')
        end
        
        builder.CdtrAgt do
          builder.FinInstnId do
            if transaction.bic.present?
              #builder.BIC(transaction.bic)
              builder.ClrSysMmbId do
                builder.ClrSysId do
                  builder.Cd('CHBCC')
                end
                builder.MmbId(transaction.bic)
              end
            end
          end
        end
        builder.Cdtr do
          builder.Nm(transaction.name)

          postladr =!transaction.street.nil? && !transaction.streetnr.nil? && !transaction.postcode.nil? && !transaction.town.nil? && !transaction.country.nil?
          adrline = !transaction.adrline1.nil? && !transaction.adrline2.nil?

          if postladr || adrline
            builder.PstlAdr do
              if postladr
                builder.StrtNm(transaction.street)
                builder.BldgNb(transaction.streetnr)
                builder.PstCd(transaction.postcode)
                builder.TwnNm(transaction.town)
                builder.Ctry(transaction.country)
              elsif adrline
                builder.AdrLine(transaction.adrline1)
                builder.AdrLine(transaction.adrline2)
              end
            end
          end
        end
        builder.CdtrAcct do
          builder.Id do
            builder.IBAN(transaction.iban)
          end
        end
        if transaction.remittance_information
          builder.RmtInf do
            builder.Ustrd(transaction.remittance_information)
          end
        end
      end
    end
  end
end