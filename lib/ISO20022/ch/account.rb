# encoding: utf-8
module ISO20022
  class CHAccount
    include ActiveModel::Validations
    extend Converter

    attr_accessor :name, :iban, :clearingcode, :clearingid, :udeb_name, :udeb_street, :udeb_streetnr, :udeb_postcode, :udeb_town, :udeb_country
    convert :name, :udeb_name, :udeb_street, :udeb_town, to: :text

    validates_length_of :name, within: 1..70
    validates_length_of :clearingcode, within: 1..10
    validates_length_of :clearingid, within: 1..10
    validates_length_of :udeb_name, within: 1..70, allow_nil: true
    validates_length_of :udeb_street, within: 1..70, allow_nil: true
    validates_length_of :udeb_streetnr, within: 1..10, allow_nil: true
    validates_length_of :udeb_postcode, within: 1..5, allow_nil: true
    validates_length_of :udeb_town, within: 1..70, allow_nil: true
    validates_length_of :udeb_country, within: 1..2, allow_nil: true
    validates_with IBANValidator

    def initialize(attributes = {})
      attributes.each do |name, value|
        public_send("#{name}=", value)
      end

      self.udeb_country = 'CH' if self.udeb_country.nil?
    end
  end
end