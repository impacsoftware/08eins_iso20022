# encoding: utf-8
module ISO20022
  class CHTransaction
    include ActiveModel::Validations
    extend Converter

    attr_accessor :name, :street, :streetnr, :postcode, :town, :country, :adrline1, :adrline2, :iban, :bic, :amount, :instruction, :reference, :remittance_information, :requested_date, :batch_booking
    convert :name, :street, :town, :adrline1, :adrline2, :instruction, :reference, :remittance_information, to: :text
    convert :amount, to: :decimal

    validates_length_of :name, within: 1..70
    validates_length_of :street, within: 1..70, allow_nil: true
    validates_length_of :streetnr, within: 1..10, allow_nil: true
    validates_length_of :postcode, within: 1..5, allow_nil: true
    validates_length_of :town, within: 1..70, allow_nil: true
    validates_length_of :country, within: 1..2, allow_nil: true
    validates_length_of :adrline1, within: 1..70, allow_nil: true
    validates_length_of :adrline2, within: 1..70, allow_nil: true
    validates_length_of :instruction, within: 1..35, allow_nil: true
    validates_length_of :reference, within: 1..35, allow_nil: true
    validates_length_of :remittance_information, within: 1..140, allow_nil: true
    validates_numericality_of :amount, greater_than: 0
    validates_inclusion_of :batch_booking, :in => [true, false]
    validates_with IBANValidator

    def initialize(attributes = {})
      attributes.each do |name, value|
        send("#{name}=", value)
      end

      self.iban = self.iban.gsub(/\s+/, "")
      self.country = 'CH' if self.country.nil?
      self.batch_booking = true if self.batch_booking.nil?
    end
  end
end