# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'ISO20022/version'

Gem::Specification.new do |spec|
  spec.name          = "ISO20022Payment"
  spec.version       = ISO20022::VERSION
  spec.authors       = ["08eins"]
  spec.email         = ["info@08eins.ch"]

  spec.summary       = 'ISO20022Payment for Switzerland'
  spec.description   = 'Based on sepa_king (https://github.com/salesking/sepa_king)'
  spec.homepage      = ''
  spec.license       = "MIT"

  spec.files         = `git ls-files -z`.split("\x0").reject { |f| f.match(%r{^(test|spec|features)/}) }
  spec.bindir        = "exe"
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ["lib"]

  spec.add_runtime_dependency 'activemodel', "~> 3.2"
  spec.add_runtime_dependency 'builder'
  spec.add_runtime_dependency 'iban-tools'

  spec.add_development_dependency "bundler", "~> 1.12"
  spec.add_development_dependency "rspec", "~> 3.0"
  spec.add_development_dependency "nokogiri"
end
